package com.xianzaishi.tms.custom.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;

public class DistributionAction extends TmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(DistributionAction.class);

	@Autowired
	private IDistributionDomainClient distributionDomainClient = null;
	@Autowired
	private UserService userService = null;

	public String getDistributionByOrderID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Result<BaseUserDTO> userResult = userService
					.queryBaseUserByToken(simpleRequestVO.getToken());
			if (userResult.getModule() == null) {
				throw new BizException("user error.");
			}
			Long orderID = Long.parseLong((String) simpleRequestVO.getData());
			flag = distributionDomainClient.getDistributionByOrderID(orderID);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IDistributionDomainClient getDistributionDomainClient() {
		return distributionDomainClient;
	}

	public void setDistributionDomainClient(
			IDistributionDomainClient distributionDomainClient) {
		this.distributionDomainClient = distributionDomainClient;
	}
}
