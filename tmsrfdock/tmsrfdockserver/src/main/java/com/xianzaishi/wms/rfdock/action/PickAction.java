package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.tmscore.rfdock.vo.TokenVO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickDetailVO;
import com.xianzaishi.wms.tmscore.vo.PickQueryVO;
import com.xianzaishi.wms.tmscore.vo.PickVO;

public class PickAction extends TmsActionAdapter {
	private static final Logger logger = Logger.getLogger(PickAction.class);
	@Autowired
	private IPickDomainClient pickDomainClient = null;

	public String submitPickedDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PickDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			List<PickDetailVO> pickDetailVOs = getPickDetails((PickDetailVO[]) simpleRequestVO
					.getData());
			PickQueryVO queryVO = new PickQueryVO();
			queryVO.setId(pickDetailVOs.get(0).getPickId());
			SimpleResultVO<List<PickVO>> pickVOs = pickDomainClient
					.queryPickVOList(queryVO);
			if (pickVOs.getTarget().get(0).getStatu() != 1) {
				throw new BizException("请确认该拣货单任务是否已完成");
			}
			flag = pickDomainClient.submitPickedDetail(pickDetailVOs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String outOfStock() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PickDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			PickDetailVO pickDetailVO = (PickDetailVO) simpleRequestVO
					.getData();
			flag = pickDomainClient.outOfStock(pickDetailVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String canclePickTask() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = pickDomainClient.cancelPickTask(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 获取该用户已有拣货任务，或为该用户分配新任务，注意并发安全
	 * 
	 * @return
	 */
	public String getPickedTask() {
		SimpleResultVO flag = null;
		try {
			PickVO pickVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			PickQueryVO queryVO = new PickQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(tokenVO.getOperator());
			queryVO.setStatu(Short.parseShort("1"));

			SimpleResultVO tmps = pickDomainClient.queryPickVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() == null
					|| ((List<PickVO>) tmps.getTarget()).isEmpty()) {
				tmps = pickDomainClient.assignPickVO(tokenVO.getAgencyID(),
						tokenVO.getOperator());
				if (!tmps.isSuccess()) {
					flag = tmps;
				} else {
					pickVO = (PickVO) tmps.getTarget();
				}
			} else {
				pickVO = ((List<PickVO>) tmps.getTarget()).get(0);
			}
			if (pickVO != null) {
				pickVO.setDetails(pickDomainClient.getPickDetailVOListByPickID(
						pickVO.getId()).getTarget());
			}
			flag = SimpleResultVO.buildSuccessResult(pickVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private List<PickDetailVO> getPickDetails(PickDetailVO[] pickDetailVOArray) {
		return Arrays.asList(pickDetailVOArray);
	}

	public IPickDomainClient getPickDomainClient() {
		return pickDomainClient;
	}

	public void setPickDomainClient(IPickDomainClient pickDomainClient) {
		this.pickDomainClient = pickDomainClient;
	}

}
