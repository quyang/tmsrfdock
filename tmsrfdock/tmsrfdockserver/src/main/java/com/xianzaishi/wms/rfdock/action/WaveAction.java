package com.xianzaishi.wms.rfdock.action;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.tmscore.rfdock.vo.TokenVO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingBasketDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingWallPositionDomainClient;
import com.xianzaishi.wms.tmscore.domain.client.itf.IWaveDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickDetailVO;
import com.xianzaishi.wms.tmscore.vo.PickQueryVO;
import com.xianzaishi.wms.tmscore.vo.PickVO;
import com.xianzaishi.wms.tmscore.vo.PickingBasketVO;
import com.xianzaishi.wms.tmscore.vo.PickingWallPositionStatuVO;
import com.xianzaishi.wms.tmscore.vo.WaveQueryVO;
import com.xianzaishi.wms.tmscore.vo.WaveVO;

public class WaveAction extends TmsActionAdapter {
	private static final Logger logger = Logger.getLogger(WaveAction.class);
	@Autowired
	private IWaveDomainClient waveDomainClient = null;
	@Autowired
	private IPickDomainClient pickDomainClient = null;
	@Autowired
	private IPickingBasketDomainClient pickingBasketDomainClient = null;
	@Autowired
	private IPickingWallPositionDomainClient pickingWallPositionDomainClient = null;

	public String getWaveVOByPickingBasketBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			flag = waveDomainClient
					.getWaveByPickingBasketID(pickingBasketDomainClient
							.getPickingBasketVOByBarcode(
									(String) simpleRequestVO.getData())
							.getTarget().getId());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getWaveVOByPickingWallPositionBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			flag = waveDomainClient
					.getWaveByWallPositionBarcode((String) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String waveFinished() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = waveDomainClient.waveFinished(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String assignPickingWallPosition() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PickingWallPositionStatuVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			PickingWallPositionStatuVO pickingWallPositionStatuVO = (PickingWallPositionStatuVO) simpleRequestVO
					.getData();
			flag = waveDomainClient.assignPickingWallPosition(
					pickingWallPositionStatuVO.getAssignmentId(),
					pickingWallPositionStatuVO.getWallPositionId());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getPackTask() {
		SimpleResultVO flag = null;
		try {
			WaveVO waveVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			WaveQueryVO queryVO = new WaveQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(tokenVO.getOperator());
			queryVO.setStatu(2);

			SimpleResultVO tmps = waveDomainClient.queryWaveVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() == null
					|| ((List<PickVO>) tmps.getTarget()).isEmpty()) {
				tmps = waveDomainClient.assignWaveTask(tokenVO.getAgencyID(),
						tokenVO.getOperator());
				if (!tmps.isSuccess()) {
					flag = tmps;
				} else {
					waveVO = (WaveVO) tmps.getTarget();
				}
			} else {
				waveVO = ((List<WaveVO>) tmps.getTarget()).get(0);
			}
			if (waveVO != null) {
				List<PickingBasketVO> pickingBaskets = new LinkedList<PickingBasketVO>();
				PickQueryVO pickQueryVO = new PickQueryVO();
				pickQueryVO.setWaveId(waveVO.getId());
				List<PickVO> pickVOs = pickDomainClient.queryPickVOList(
						pickQueryVO).getTarget();
				waveVO.setPickVOs(pickVOs);
				for (int i = 0; i < pickVOs.size(); i++) {
					pickingBaskets
							.addAll(pickDomainClient
									.getPickingBasketVOsByPickID(
											pickVOs.get(i).getId()).getTarget());
				}
				waveVO.setPickingBaskets(pickingBaskets);
				List<PickDetailVO> pickDetailVOs = new LinkedList<PickDetailVO>();
				for (int i = 0; i < pickVOs.size(); i++) {
					pickDetailVOs
							.addAll(pickDomainClient
									.getPickDetailVOListByPickID(
											pickVOs.get(i).getId()).getTarget());
				}
				waveVO.setPickDetails(pickDetailVOs);
				waveVO.setDetails(waveDomainClient.getWaveDetailVOListByWaveID(
						waveVO.getId()).getTarget());
				waveVO.setPickingWallPositions(pickingWallPositionDomainClient
						.getPickingWallPositionByWaveID(waveVO.getId())
						.getTarget());
				flag = SimpleResultVO.buildSuccessResult(waveVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String realsePackTask() {
		SimpleResultVO flag = null;
		try {
			WaveVO waveVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			WaveQueryVO queryVO = new WaveQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(tokenVO.getOperator());
			queryVO.setStatu(2);

			SimpleResultVO tmps = waveDomainClient.queryWaveVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() != null
					&& !((List<PickVO>) tmps.getTarget()).isEmpty()) {
				waveVO = ((List<WaveVO>) tmps.getTarget()).get(0);
				flag = waveDomainClient.releaseWaveTask(waveVO.getId());
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IWaveDomainClient getWaveDomainClient() {
		return waveDomainClient;
	}

	public void setWaveDomainClient(IWaveDomainClient waveDomainClient) {
		this.waveDomainClient = waveDomainClient;
	}

	public IPickDomainClient getPickDomainClient() {
		return pickDomainClient;
	}

	public void setPickDomainClient(IPickDomainClient pickDomainClient) {
		this.pickDomainClient = pickDomainClient;
	}

	public IPickingBasketDomainClient getPickingBasketDomainClient() {
		return pickingBasketDomainClient;
	}

	public void setPickingBasketDomainClient(
			IPickingBasketDomainClient pickingBasketDomainClient) {
		this.pickingBasketDomainClient = pickingBasketDomainClient;
	}

	public IPickingWallPositionDomainClient getPickingWallPositionDomainClient() {
		return pickingWallPositionDomainClient;
	}

	public void setPickingWallPositionDomainClient(
			IPickingWallPositionDomainClient pickingWallPositionDomainClient) {
		this.pickingWallPositionDomainClient = pickingWallPositionDomainClient;
	}
}
