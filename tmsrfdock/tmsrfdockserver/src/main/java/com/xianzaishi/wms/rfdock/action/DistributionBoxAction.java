package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionBoxDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionBoxVO;

public class DistributionBoxAction extends TmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(DistributionBoxAction.class);

	@Autowired
	private IDistributionBoxDomainClient distributionBoxDomainClient = null;

	public String getDistributionBoxByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			String barcode = (String) simpleRequestVO.getData();
			flag = distributionBoxDomainClient
					.getDistributionBoxVOByBarcode(barcode);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getDistributionBoxVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Long id = Long.parseLong((String) simpleRequestVO.getData());
			flag = distributionBoxDomainClient.getDistributionBoxVOByID(id);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetDistributionBoxVOByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> barcodes = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<String> sbarcodes = new LinkedList<String>();
			for (Object object : barcodes) {
				sbarcodes.add((String) object);
			}
			flag = distributionBoxDomainClient
					.batchGetDistributionBoxVOByBarcode(sbarcodes);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetDistributionBoxVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> ids = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<Long> sids = new LinkedList<Long>();
			for (Object object : ids) {
				sids.add(Long.parseLong((String) object));
			}
			flag = distributionBoxDomainClient
					.batchGetDistributionBoxVOByID(sids);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDistributionBox() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(DistributionBoxVO.class);
			flag = distributionBoxDomainClient
					.addDistributionBoxVO((DistributionBoxVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IDistributionBoxDomainClient getDistributionBoxDomainClient() {
		return distributionBoxDomainClient;
	}

	public void setDistributionBoxDomainClient(
			IDistributionBoxDomainClient distributionBoxDomainClient) {
		this.distributionBoxDomainClient = distributionBoxDomainClient;
	}

}
