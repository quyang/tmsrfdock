package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingWallPositionDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickingWallPositionVO;

public class PickingWallPositionAction extends TmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(PickingWallPositionAction.class);

	@Autowired
	private IPickingWallPositionDomainClient pickingWallPositionDomainClient = null;

	public String getPickingWallPositionByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			String barcode = (String) simpleRequestVO.getData();
			flag = pickingWallPositionDomainClient
					.getPickingWallPositionVOByBarcode(barcode);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getPickingWallPositionVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Long id = Long.parseLong((String) simpleRequestVO.getData());
			flag = pickingWallPositionDomainClient
					.getPickingWallPositionVOByID(id);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetPickingWallPositionVOByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> barcodes = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<String> sbarcodes = new LinkedList<String>();
			for (Object object : barcodes) {
				sbarcodes.add((String) object);
			}
			flag = pickingWallPositionDomainClient
					.batchGetPickingWallPositionVOByBarcode(sbarcodes);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetPickingWallPositionVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> ids = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<Long> sids = new LinkedList<Long>();
			for (Object object : ids) {
				sids.add(Long.parseLong((String) object));
			}
			flag = pickingWallPositionDomainClient
					.batchGetPickingWallPositionVOByID(sids);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addPickingWallPosition() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PickingWallPositionVO.class);
			flag = pickingWallPositionDomainClient
					.addPickingWallPositionVO((PickingWallPositionVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IPickingWallPositionDomainClient getPickingWallPositionDomainClient() {
		return pickingWallPositionDomainClient;
	}

	public void setPickingWallPositionDomainClient(
			IPickingWallPositionDomainClient pickingWallPositionDomainClient) {
		this.pickingWallPositionDomainClient = pickingWallPositionDomainClient;
	}

}
