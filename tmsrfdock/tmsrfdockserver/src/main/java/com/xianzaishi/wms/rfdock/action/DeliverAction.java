package com.xianzaishi.wms.rfdock.action;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.tmscore.rfdock.vo.TokenVO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDeliverDomainClient;
import com.xianzaishi.wms.tmscore.vo.DeliverDetailVO;
import com.xianzaishi.wms.tmscore.vo.DeliverDistributionVO;
import com.xianzaishi.wms.tmscore.vo.DeliverQueryVO;
import com.xianzaishi.wms.tmscore.vo.DeliverVO;
import com.xianzaishi.wms.tmscore.vo.DistributionBoxStatuVO;
import com.xianzaishi.wms.tmscore.vo.DistributionBoxVO;
import com.xianzaishi.wms.tmscore.vo.PickVO;

public class DeliverAction extends TmsActionAdapter {

	private static final Logger logger = Logger.getLogger(DeliverAction.class);
	@Autowired
	private IDeliverDomainClient deliverDomainClient = null;

	public String getDeliverDistributions() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = deliverDomainClient.getDeliverDistributions(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getDeliverTask() {
		SimpleResultVO flag = null;
		try {
			DeliverVO deliverVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverQueryVO queryVO = new DeliverQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(tokenVO.getOperator());
			queryVO.setStatu(1);
			SimpleResultVO tmps = deliverDomainClient
					.queryDeliverVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() == null
					|| ((List<PickVO>) tmps.getTarget()).isEmpty()) {
				tmps = deliverDomainClient.assignDeliverVO(
						tokenVO.getAgencyID(), tokenVO.getOperator());
				if (!tmps.isSuccess()) {
					flag = tmps;
				} else {
					deliverVO = (DeliverVO) tmps.getTarget();
				}
			} else {
				deliverVO = ((List<DeliverVO>) tmps.getTarget()).get(0);
			}
			if (deliverVO != null) {
				deliverVO.setDetails(deliverDomainClient
						.getDeliverDetailVOListByDeliverID(deliverVO.getId())
						.getTarget());

				deliverVO.setDistributions(deliverDomainClient
						.getDeliverDistributionVOByDeliverID(deliverVO.getId())
						.getTarget());
				flag = SimpleResultVO.buildSuccessResult(deliverVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String assignDeliverTask() {
		SimpleResultVO flag = null;
		try {
			DeliverVO deliverVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverQueryVO queryVO = new DeliverQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(Long.parseLong((String) simpleRequestVO
					.getData()));
			queryVO.setStatu(1);
			SimpleResultVO tmps = deliverDomainClient
					.queryDeliverVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() == null
					|| ((List<PickVO>) tmps.getTarget()).isEmpty()) {
				tmps = deliverDomainClient.assignDeliverVO(
						tokenVO.getAgencyID(), queryVO.getOperator());
				if (!tmps.isSuccess()) {
					flag = tmps;
				} else {
					deliverVO = (DeliverVO) tmps.getTarget();
				}
			} else {
				deliverVO = ((List<DeliverVO>) tmps.getTarget()).get(0);
			}
			if (deliverVO != null) {
				deliverVO.setDetails(deliverDomainClient
						.getDeliverDetailVOListByDeliverID(deliverVO.getId())
						.getTarget());

				deliverVO.setDistributions(deliverDomainClient
						.getDeliverDistributionVOByDeliverID(deliverVO.getId())
						.getTarget());
				flag = SimpleResultVO.buildSuccessResult(deliverVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getUserDeliverTask() {
		SimpleResultVO flag = null;
		try {
			DeliverVO deliverVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverQueryVO queryVO = new DeliverQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(Long.parseLong((String) simpleRequestVO
					.getData()));
			queryVO.setStatu(1);
			SimpleResultVO tmps = deliverDomainClient
					.queryDeliverVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() != null
					&& !((List<PickVO>) tmps.getTarget()).isEmpty()) {
				deliverVO = ((List<DeliverVO>) tmps.getTarget()).get(0);
			}
			if (deliverVO != null) {
				deliverVO.setDetails(deliverDomainClient
						.getDeliverDetailVOListByDeliverID(deliverVO.getId())
						.getTarget());

				deliverVO.setDistributions(deliverDomainClient
						.getDeliverDistributionVOByDeliverID(deliverVO.getId())
						.getTarget());
				List<DistributionBoxVO> boxes = new LinkedList<DistributionBoxVO>();
				List<Long> boxIDs = new LinkedList<Long>();
				for (int i = 0; i < deliverVO.getDetails().size(); i++) {
					if (!boxIDs.contains(((DeliverDetailVO) deliverVO
							.getDetails().get(i)).getBoxId())) {
						boxes.add(deliverDomainClient
								.getDistriButionBoxVOByBoxID(
										((DeliverDetailVO) deliverVO
												.getDetails().get(i))
												.getBoxId()).getTarget());
						boxIDs.add(((DeliverDetailVO) deliverVO.getDetails()
								.get(i)).getBoxId());
					}
				}

				deliverVO.setBoxes(boxes);

				flag = SimpleResultVO.buildSuccessResult(deliverVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getUserDeliverTaskOngoing() {
		SimpleResultVO flag = null;
		try {
			DeliverVO deliverVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverQueryVO queryVO = new DeliverQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(Long.parseLong((String) simpleRequestVO
					.getData()));
			queryVO.setStatu(2);
			SimpleResultVO tmps = deliverDomainClient
					.queryDeliverVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() != null
					&& !((List<PickVO>) tmps.getTarget()).isEmpty()) {
				deliverVO = ((List<DeliverVO>) tmps.getTarget()).get(0);
			}
			if (deliverVO != null) {
				deliverVO.setDetails(deliverDomainClient
						.getDeliverDetailVOListByDeliverID(deliverVO.getId())
						.getTarget());

				deliverVO.setDistributions(deliverDomainClient
						.getDeliverDistributionVOByDeliverID(deliverVO.getId())
						.getTarget());
				List<DistributionBoxVO> boxes = new LinkedList<DistributionBoxVO>();
				List<Long> boxIDs = new LinkedList<Long>();
				for (int i = 0; i < deliverVO.getDetails().size(); i++) {
					if (!boxIDs.contains(((DeliverDetailVO) deliverVO
							.getDetails().get(i)).getBoxId())) {
						boxes.add(deliverDomainClient
								.getDistriButionBoxVOByBoxID(
										((DeliverDetailVO) deliverVO
												.getDetails().get(i))
												.getBoxId()).getTarget());
						boxIDs.add(((DeliverDetailVO) deliverVO.getDetails()
								.get(i)).getBoxId());
					}
				}

				deliverVO.setBoxes(boxes);

				flag = SimpleResultVO.buildSuccessResult(deliverVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String handOverDeliver() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(DeliverVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverVO deliverVO = (DeliverVO) simpleRequestVO.getData();

			flag = deliverDomainClient.handOverDeliver(deliverVO.getId());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String returnDeliverBox() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", DeliverVO.class);
			classMap.put("details", DeliverDetailVO.class);
			classMap.put("boxes", DistributionBoxStatuVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<DeliverVO> simpleRequestVO = (SimpleRequestVO<DeliverVO>) JSONObject
					.toBean(tmp, SimpleRequestVO.class, classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			flag = deliverDomainClient
					.deliverBoxReturn((DeliverVO) simpleRequestVO.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitDeliver() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", DeliverVO.class);
			classMap.put("details", DeliverDetailVO.class);
			classMap.put("distributions", DeliverDistributionVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<DeliverVO> simpleRequestVO = (SimpleRequestVO<DeliverVO>) JSONObject
					.toBean(tmp, SimpleRequestVO.class, classMap);

			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			DeliverVO deliverVO = simpleRequestVO.getData();
			deliverVO.setAgencyId(tokenVO.getAgencyID());
			flag = deliverDomainClient.submitDeliver(deliverVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IDeliverDomainClient getDeliverDomainClient() {
		return deliverDomainClient;
	}

	public void setDeliverDomainClient(IDeliverDomainClient deliverDomainClient) {
		this.deliverDomainClient = deliverDomainClient;
	}

}
