package com.xianzaishi.wms.rfdock.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionDetailVO;
import com.xianzaishi.wms.tmscore.vo.DistributionVO;

public class DistributionAction extends TmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(DistributionAction.class);

	@Autowired
	private IDistributionDomainClient distributionDomainClient = null;

	public String getDistributionVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Long id = Long.parseLong((String) simpleRequestVO.getData());
			flag = distributionDomainClient.getDistributionVOByID(id);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String distributionArrived() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", DistributionVO.class);
			classMap.put("details", DistributionDetailVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<DistributionVO> simpleRequestVO = (SimpleRequestVO<DistributionVO>) JSONObject
					.toBean(tmp, SimpleRequestVO.class, classMap);
			flag = distributionDomainClient.arrived(simpleRequestVO.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String distributionRejected() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Long id = Long.parseLong((String) simpleRequestVO.getData());
			flag = distributionDomainClient.rejected(id);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IDistributionDomainClient getDistributionDomainClient() {
		return distributionDomainClient;
	}

	public void setDistributionDomainClient(
			IDistributionDomainClient distributionDomainClient) {
		this.distributionDomainClient = distributionDomainClient;
	}
}
