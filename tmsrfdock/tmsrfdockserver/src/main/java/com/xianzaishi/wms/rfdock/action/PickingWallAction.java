package com.xianzaishi.wms.rfdock.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.tmscore.common.action.TmsActionAdapter;
import com.xianzaishi.tmscore.rfdock.vo.TokenVO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickQueryVO;
import com.xianzaishi.wms.tmscore.vo.PickVO;

public class PickingWallAction extends TmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(PickingWallAction.class);
	@Autowired
	private IPickDomainClient pickDomainClient = null;

	public String recievePickingBasket() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			String pickingBasketBarcode = (String) simpleRequestVO.getData();

			pickDomainClient.recievePickingBasket(pickingBasketBarcode);

			flag = SimpleResultVO.buildSuccessResult(true);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 获取该用户已有拣货任务，或为该用户分配新任务，注意并发安全
	 * 
	 * @return
	 */
	public String getPickedTask() {
		SimpleResultVO flag = null;
		try {
			PickVO pickVO = null;
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			PickQueryVO queryVO = new PickQueryVO();
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setOperator(tokenVO.getOperator());
			queryVO.setStatu(Short.parseShort("1"));

			SimpleResultVO tmps = pickDomainClient.queryPickVOList(queryVO);

			if (!tmps.isSuccess()) {
				flag = tmps;
			} else if (tmps.getTarget() == null
					|| ((List<PickVO>) tmps.getTarget()).isEmpty()) {
				tmps = pickDomainClient.assignPickVO(tokenVO.getAgencyID(),
						tokenVO.getOperator());
				if (!tmps.isSuccess()) {
					flag = tmps;
				} else {
					pickVO = (PickVO) tmps.getTarget();
				}
			} else {
				pickVO = ((List<PickVO>) tmps.getTarget()).get(0);
			}
			if (pickVO != null) {
				pickVO.setDetails(pickDomainClient.getPickDetailVOListByPickID(
						pickVO.getId()).getTarget());

				flag = SimpleResultVO.buildSuccessResult(pickVO);
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IPickDomainClient getPickDomainClient() {
		return pickDomainClient;
	}

	public void setPickDomainClient(IPickDomainClient pickDomainClient) {
		this.pickDomainClient = pickDomainClient;
	}

}
