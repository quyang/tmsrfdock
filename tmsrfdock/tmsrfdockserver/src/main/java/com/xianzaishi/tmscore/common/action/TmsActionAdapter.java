package com.xianzaishi.tmscore.common.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.purchasecenter.client.organization.OrganizationService;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.tmscore.rfdock.vo.TokenVO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;

public abstract class TmsActionAdapter extends ActionAdapter {

	private static final String TOKENKEY = "token";

	@Autowired
	protected BackGroundUserService backgrounduserservice = null;
	@Autowired
	protected OrganizationService organizationService = null;

	protected TokenVO getTokenInfo(String token) {
		if (token == null || token.isEmpty()) {
			throw new BizException("token is empty");
		}
		BackGroundUserDTO backGroundUserDTO = backgrounduserservice
				.queryUserDTOByToken(token).getModule();
		if (backGroundUserDTO == null) {
			throw new BizException("no user got");
		}
		TokenVO tokenVO = new TokenVO();
		tokenVO.setAgencyID(1l);
		tokenVO.setGovID(1l);
		tokenVO.setOperator(new Long(backGroundUserDTO.getUserId()));
		if (tokenVO == null) {
			throw new BizException("没有获取到d登录信息");
		}
		return tokenVO;
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public BackGroundUserService getBackgrounduserservice() {
		return backgrounduserservice;
	}

	public void setBackgrounduserservice(
			BackGroundUserService backgrounduserservice) {
		this.backgrounduserservice = backgrounduserservice;
	}

}
